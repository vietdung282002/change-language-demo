package com.example.change_language_demo

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.Locale

const val TAG = "Activity Lifecycle"

class MainActivity : AppCompatActivity() {

    private lateinit var helloTV: TextView
    private lateinit var currentLanguageTV: TextView
    private lateinit var vietnameseBtn: Button
    private lateinit var frenchBtn: Button
    private lateinit var englishBtn: Button
    private lateinit var editText: EditText



    override fun attachBaseContext(newBase: Context?) {
        val localeToSwitch = Locale(ContextUtils.language)
        val localeUpdated = newBase?.let {
            ContextUtils.updateLocale(it, localeToSwitch)
        }
        super.attachBaseContext(localeUpdated)
        Log.d(TAG, "attachBaseContext: ")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        helloTV = findViewById(R.id.hello)
        currentLanguageTV = findViewById(R.id.content)
        vietnameseBtn = findViewById(R.id.vi_btn)
        englishBtn = findViewById(R.id.en_btn)
        frenchBtn = findViewById(R.id.fr_btn)
        editText = findViewById(R.id.editText)
        val intent = intent
        val bundle = intent.extras

        if(savedInstanceState != null){
            editText.setText(savedInstanceState.getString("current"))
        }else if (bundle != null){
            editText.setText(bundle.getString("current"))
        }


        vietnameseBtn.setOnClickListener {
            if(ContextUtils.language != "vi"){
                changeLanguage(vietnameseBtn)
            }
        }

        frenchBtn.setOnClickListener {
            if(ContextUtils.language != "fr"){
                changeLanguage(frenchBtn)
            }
        }

        englishBtn.setOnClickListener {
            if(ContextUtils.language != "en"){
                changeLanguage(englishBtn)
            }
        }
        Log.d(TAG, "onCreate: ")
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy: ")
        super.onDestroy()
    }
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("current",editText.text.toString())
        Log.d(TAG, "onSaveInstanceState: ")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop: ")
    }

    private fun changeLanguage(view: View) {
        when(view){
            vietnameseBtn -> {
                ContextUtils.language = "vi"
            }
            frenchBtn -> {
                ContextUtils.language = "fr"
            }
            englishBtn -> {
                ContextUtils.language = "en"
            }
        }
        val intent = Intent(this, MainActivity:: class.java)
        val bundle = Bundle()
        bundle.putString("current", editText.text.toString())
        intent.putExtras(bundle)
        startActivity(intent)
        finish()
    }
}